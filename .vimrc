"------------------------------------------------------------------------------
"   General Options
"------------------------------------------------------------------------------
let mapleader = " "
syntax on
set exrc
set nohlsearch
set noerrorbells
set tabstop=4 softtabstop=4
set expandtab
set smartindent
set nowrap
set smartcase
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set incsearch
set colorcolumn=80
set number
set relativenumber
set background=dark
set ruler
set hidden
"set termguicolors " does not work with vim only with nvim on mac os
set scrolloff=8
set backspace=indent,eol,start
set signcolumn=yes
set updatetime=50

autocmd InsertEnter,InsertLeave * set cul!
set ttimeout
set ttimeoutlen=1


"------------------------------------------------------------------------------
"   Key Mappings
"------------------------------------------------------------------------------

" --- Brackets autocompletion -----
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

"------------------------------------------------------------------------------
"   Plugins
"------------------------------------------------------------------------------

" autoload plug in manager if not already installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
	silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

Plug 'psf/black', { 'branch': 'stable' }
Plug 'vim-scripts/indentpython.vim'
Plug 'morhetz/gruvbox'
Plug 'sheerun/vim-polyglot'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Initialize plugin system
call plug#end()

"------------------------------------------------------------------------------
"   Plugin Options
"------------------------------------------------------------------------------

" black autoformat 
autocmd BufWritePre *.py execute ':Black'


"------------------------------------------------------------------------------
"   Colors and Themes
"------------------------------------------------------------------------------

colorscheme gruvbox
